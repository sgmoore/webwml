#use wml::debian::template title="Debian GNU/Linux 2.2 -- Errata" BARETITLE=true
#use wml::debian::translation-check translation="534d1b782cfb92f46dc41fd064f779fffc329b12"

<p><strong>Debian GNU/Linux 2.2 werd vervangen door
<a href="../woody/">Debian GNU/Linux 3.0 ("woody")</a>.
Beveiligingsupdates zijn stopgezet vanaf 30 juni 2003.</strong>
Raadpleeg
<a href="https://lists.debian.org/debian-devel-announce/2003/debian-devel-announce-200302/msg00010.html">\
de resultaten van de enquête van het beveiligingsteam</a> voor meer informatie.</p>

<h2>Beveiligingszaken</h2>

<p>Het Debian-beveiligingsteam zorgt voor updates van pakketten in de stabiele
release voor geïdentificeerde beveiligingsproblemen. Bekijk de
<a href="$(HOME)/security/">beveiligingspagina's</a> voor informatie over
alle geïdentificeerde beveiligingszaken in `potato'.</p>

<p>Als u APT gebruikt, voeg dan volgende regel toe aan <tt>/etc/apt/sources.list</tt>
om toegang te hebben tot de laatste beveiligingsupdates:</p>

<pre>
  deb http://security.debian.org/ potato/updates main contrib non-free
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>

<h2>Tussenreleases</h2>

<p>Soms, in geval van verschillende kritieke problemen of beveiligingsupdates
wordt de uitgebrachte distributie bijgewerkt. Normaal gesproken worden ze
aangeduid als tussenreleases.</p>

<ul>
  <li>De eerste tussenrelease, 2.2r1, werd uitgebracht op <a href="$(HOME)/News/2000/20001114">14 november 2000</a>.</li>
  <li>De tweede tussenrelease, 2.2r2, werd uitgebracht op <a href="$(HOME)/News/2000/20001205">3 december 2000</a>.</li>
  <li>De derde tussenrelease, 2.2r3, werd uitgebracht op <a href="$(HOME)/News/2001/20010417">17 april 2001</a>.</li>
  <li>De vierde tussenrelease, 2.2r4, werd uitgebracht op <a href="$(HOME)/News/2001/20011105">5 november 2001</a>.</li>
  <li>De vijfde tussenrelease, 2.2r5, werd uitgebracht op <a href="$(HOME)/News/2002/20020110">10 january 2002</a>.</li>
  <li>De zesde tussenrelease, 2.2r6, werd uitgebracht op <a href="$(HOME)/News/2002/20020403">3 april 2002</a>.</li>
  <li>De zevende tussenrelease, 2.2r7, werd uitgebracht op <a href="$(HOME)/News/2002/20020713">13 july 2002</a>.</li>
</ul>

<p>
Zie de <a href="http://archive.debian.org/debian/dists/potato/ChangeLog">
ChangeLog</a> (en
<a href="http://archive.debian.org/debian-non-US/dists/potato/non-US/ChangeLog">
ChangeLog voor non-US</a>) voor details over de wijzigingen.</p>

<p>Verbeteringen in de uitgebrachte stabiele distributie gaan regelmatig door
een uitgebreide testperiode alvorens ze worden aanvaard in het archief.
Echter, deze verbeteringen zijn wel beschikbaar in de
<a href="http://archive.debian.org/debian/dists/potato-proposed-updates/">
dists/potato-proposed-updates</a>-map van elke Debian-archiefspiegelserver
(en op dezelfde lokatie op onze
<a href="http://archive.debian.org/debian-non-US/dists/potato-proposed-updates/">
non-US server</a> en zijn spiegelservers).</p>

<p>Als u <tt>apt</tt> gebruikt voor uw pakketupdates, kunt u de
voorgestelde updates installeren door de volgende regel toe te voegen aan
<tt>/etc/apt/sources.list</tt>:</p>

# These links should probably be localized in translated versions,
# if possible.
<pre>
  \# voorgestelde updates voor een 2.2 tussenrelease
  deb http://archive.debian.org dists/potato-proposed-updates/
  deb http://archive.debian.org/debian-non-US dists/potato-proposed-updates/
</pre>

<p>Voer daarna <kbd>apt-get update</kbd> uit, gevolgd door
<kbd>apt-get upgrade</kbd>.</p>

# someone please verify if this is still true for 2.2r3:
#
# <h2>Building CD images</h2>
#
# <p>People wishing to create custom CD images for 2.2r2 will need to use
# the CVS version of `debian-cd' and <strong>not</strong> the version currently
# installed in potato.</p>

<h2>Niet-officiële ondersteuning voor een geïnternationaliseerde installatie</h2>

<p>Een geïnternationaliseerde versie van het installatiesysteem voor i386 is
beschikbaar (https://people.debian.org/~aph/current-i18n/) voor testdoeleinden
en voor gebruik. U moet de versies 'idepci' of 'compact' gebruiken.</p>

<p>Omdat het geïnternationaliseerd installatiesysteem nog met een aantal
problemen kampt, is het onwaarschijnlijk dat het officieel zal opgenomen worden
in een tussenrelease van Potato. We kunnen alleen maar hopen dat we het voor
elkaar krijgen voor <a href="../woody/">woody</a>.</p>


<h2>Niet-officiële ondersteuning voor ReiserFS</h2>

<p>Er schijnen ten minste twee niet-officiële versies van het
installatiesysteem voor i386 te zijn die ReiserFS ondersteunen.</p>

<p>De ene versie is van John H. Robinson, IV, en is beschikbaar met enkele
instructies op <a
href="http://chao.ucsd.edu/debian/boot-floppies/">
http://chao.ucsd.edu/debian/boot-floppies/</a>.</p>

<p>De andere is van Marc A. Volovic en is beschikbaar op
http://www.bard.org.il/~marc/linux/rfs/.</p>


<h2>Niet-officiële ondersteuning voor de Linux Kernel 2.4</h2>

<p>
Adrian Bunk heeft gezorgd voor een aantal pakketten voor i386 Potato-systemen
die compatibiliteit bieden voor de Linux Kernel 2.4.x. Lees <a
href="http://www.fs.tum.de/~bunk/kernel-24.html">
http://www.fs.tum.de/~bunk/kernel-24.html</a> voor instructies,
waarschuwingen en aan <code>/etc/apt/sources.list</code> toe te voegen regels.</p>
