#use wml::debian::translation-check translation="4ac8e507e1e9c92535a9ab99c8440d1a1d7a4e4d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Busybox, un programme d’utilitaires pour les petits systèmes embarqués,
était affecté par plusieurs vulnérabilités de sécurité. Le projet « Common
vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2011-5325">CVE-2011-5325</a>

<p>Une vulnérabilité de traversée de répertoires a été découverte dans
l’implémentation dans Busybox de tar. tar extrayait un lien symbolique pointant
en dehors du répertoire de travail utilisé et puis suivait ce lien symbolique
lors de l’extraction d’autres fichiers. Cela permettait une attaque par
traversée de répertoires lors de l’extraction d’archives compressées non
fiables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-1813">CVE-2013-1813</a>

<p>Lorsque un nœud de périphérique ou un lien symbolique dans /dev devait être
créé dans un sous-répertoire de profondeur deux ou plus
(/dev/dir1/dir2.../nœud), les répertoires intermédiaires étaient créés avec des
permissions incorrectes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4607">CVE-2014-4607</a>

<p>Un dépassement d'entier pouvait se produire lors du traitement de n’importe
quelle variante de <q>littéral exécution</q> dans la fonction
lzo1x_decompress_safe. Chacun de ces trois emplacements est sujet à un
dépassement d'entier lors du traitement d’octets zéro. Cela expose le code qui
copie ces littérals à une corruption de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9645">CVE-2014-9645</a>

<p>La fonction add_probe dans modutils/modprobe.c dans BusyBox permettait à des
utilisateurs locaux de contourner les restrictions voulues lors du chargement
de modules de noyau à l’aide d'un caractère <q>/</q> (barre oblique) dans un
nom de module, comme le montre une commande <q>ifconfig /usbserial up</q> ou
<q>mount -t /snd_pcm none</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2147">CVE-2016-2147</a>

<p>Un dépassement d'entier dans le client DHCP (udhcpc) dans BusyBox permet à des
attaquants distants de provoquer un déni de service (plantage) à l'aide d'un
nom de domaine encodé suivant la RFC1035 et mal formé, qui déclenche une
écriture de tas hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2148">CVE-2016-2148</a>

<p>Un dépassement de tampon de tas dans le client DHCP client (udhcpc) dans
BusyBox permettait à des attaquants distants d’avoir un impact non précisé
à l’aide de vecteurs impliquant l’analyse d’OPTION_6RD.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15873">CVE-2017-15873</a>

<p>La fonction get_next_block dans archival/libarchive
/decompress_bunzip2.c dans BusyBox avait un dépassement d'entier qui pouvait
conduire à une violation de l’accès en écriture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-16544">CVE-2017-16544</a>

<p>Dans la fonction add_match dans libbb/lineedit.c dans BusyBox, la
fonctionnalité d’autocomplétion de l’interpréteur de commandes, utilisée pour
obtenir une liste de noms de fichiers d’un répertoire, ne nettoyait pas ces noms
et aboutissait dans l’exécution de n’importe quelle séquence d’échappement dans
le terminal. Cela pouvait éventuellement résulter dans une exécution de code,
une écriture de fichier arbitraire ou d’autres attaques.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000517">CVE-2018-1000517</a>

<p>BusyBox contenait une vulnérabilité de dépassement de tampon dans sa version de
wget qui pouvait aboutir à un dépassement de tampon de tas. Cette attaque semble
exploitable à l’aide d’une connexion réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-9621">CVE-2015-9621</a>

<p>La décompression d’un fichier zip contrefait spécialement résultait dans un
calcul de pointeur non valable et à un plantage en lisant une adresse non
valable.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.22.0-19+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets busybox.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de busybox, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/busybox">\
https://security-tracker.debian.org/tracker/busybox</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2559.data"
# $Id: $
