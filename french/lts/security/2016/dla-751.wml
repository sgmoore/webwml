#use wml::debian::translation-check translation="c8457a59a89d51637f9c29eabc2b64c8a52130b6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que Nagios était vulnérable à deux problèmes de
sécurité qui, lorsqu'ils sont combinés, mènent à une vulnérabilité
d'exécution distante de code par le superutilisateur. Heureusement, le
renforcement des permissions du paquet Debian limite leur effet à une
divulgation d'informations, mais une élévation de privilèges à ceux du
superutilisateur est encore possible localement.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9565">CVE-2016-9565</a>

<p>Une vérification incorrecte des entrées de flux RSS permet la lecture et
l'écriture à distance sans authentification de fichiers arbitraires, ce qui
pourrait conduire à une exécution distante de code si le répertoire web
racine est accessible en écriture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9566">CVE-2016-9566</a>

<p>Un traitement non sûr du fichier de journalisation permet à des
utilisateurs non privilégiés d'élever leurs privilèges à ceux du
superutilisateur. Dans Wheezy, Cela n'est possible qu'à travers le fichier
journal de débogage qui est désactivé par défaut.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.4.1-3+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nagios3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-751.data"
# $Id: $
