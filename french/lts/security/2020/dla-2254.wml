#use wml::debian::translation-check translation="00e0de32a064c832a621942816621011e4b30605" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2020-14929">CVE-2020-14929</a></p>

<p>Alpine, dans ses versions antérieures à 2.23, continue à utiliser une
connexion non sécurisée, sans avertissement, après l'envoi d’un /tls dans
certaines circonstances impliquant PREAUTH, ce qui est un comportement moins
sûr que l’alternative de fermer la connexion et laisser l’utilisateur
décider de ce qu’il voudrait faire.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 2.11+dfsg1-3+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets alpine.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2254.data"
# $Id: $
