#use wml::debian::translation-check translation="1d785510dd9f04a418ee8ea6ad400756f9ca3128" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42852">CVE-2022-42852</a>

<p>hazbinhotel a découvert que le traitement d’un contenu web contrefait de
manière malveillante pouvait aboutir à une divulgation de la mémoire du
processus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42856">CVE-2022-42856</a>

<p>Clement Lecigne a découvert que le traitement d’un contenu web contrefait de
manière malveillante pouvait aboutir à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42867">CVE-2022-42867</a>

<p>Maddie Stone a découvert que le traitement d’un contenu web contrefait de
manière malveillante pouvait aboutir à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46692">CVE-2022-46692</a>

<p>KirtiKumar Anandrao Ramchandani a découvert que le traitement d’un contenu
web contrefait de manière malveillante pouvait aboutir à un contournement de la
politique de même origine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46698">CVE-2022-46698</a>

<p>Dohyun Lee et Ryan Shin ont découvert que le traitement d’un contenu web
contrefait de manière malveillante pouvait aboutir à la divulgation
d’informations sensibles sur l’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46699">CVE-2022-46699</a>

<p>Samuel Gross a découvert que le traitement d’un contenu web contrefait de
manière malveillante pouvait aboutir à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46700">CVE-2022-46700</a>

<p>Samuel Gross a découvert que le traitement d’un contenu web contrefait de
manière malveillante pouvait aboutir à l’exécution de code arbitraire.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.38.3-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3274.data"
# $Id: $
