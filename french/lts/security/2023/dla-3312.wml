#use wml::debian::translation-check translation="5444263f277d742e990095a2995a848251f47843" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette publication corrige divers problèmes dans le chargeur d’amorçage shim
et met à jour vers une version entretenue. Les anciennes versions de shim
peuvent être un jour bloquées par le Secure Boot, aussi, sur les systèmes avec
le Secure Boot activé, il est fortement recommandé de mettre à niveau vers
cette nouvelle version pour garder le système amorçable.</p>

<p>De plus, cette mise à jour bloque les anciennes versions non sûres de GRUB.
Par conséquent, une mise à niveau vers un paquet 2.06-3~deb10u3 signé de GRUB,
comme signalé dans la DLA 3190-2, doit être réalisée avant de mettre à jour le
paquet shim.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 15.7-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets shim.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de shim,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/shim">\
https://security-tracker.debian.org/tracker/shim</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3312.data"
# $Id: $
