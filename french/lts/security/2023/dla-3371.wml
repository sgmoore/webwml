#use wml::debian::translation-check translation="255b07ac34425e9c61b4cb8e52d25027ccaf1090" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans unbound, un
résolveur DNS mettant en cache, récursif et validant.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3204">CVE-2022-3204</a>

<p>Une vulnérabilité appelée <q>Non-Responsive Delegation Attack</q>
(NRDelegation Attack) a été découverte dans divers logiciels de résolution DNS.
L’attaque NRDelegation fonctionne en ayant une délégation malveillante avec un
nombre considérable de serveurs de noms non réactifs. L’attaque débute en
demandant à un résolveur un enregistrement qui repose sur un de ces serveurs de
noms non réactifs. L’attaque peut faire qu’un résolveur prenne un temps très
long ou utilise d’importantes ressources pour résoudre des enregistrements sous
un point de délégation malveillante où réside un nombre considérable
d’enregistrements de serveurs non réactifs. Cela peut déclencher un usage
intensif du CPU dans certaines implémentations de résolveur qui recherchent
continuellement dans le cache pour des enregistrements résolus de serveurs de
noms dans cette délégation. Cela peut conduire à une dégradation des
performances et finalement à un déni de service lors d’attaques orchestrées.
Unbound n’est pas sensible à un usage intense de CPU, mais des ressources sont
toujours nécessaires pour résoudre la délégation malveillante. Unbound essaiera
de résoudre l’enregistrement jusqu’à ce que les limites extrêmes soient atteintes.
Selon la nature de l’attaque et des réponses, différentes limites pouvaient être
atteintes. Désormais Unbound incorpore des correctifs pour une meilleure
performance lorsque surchargé en coupant les requêtes opportunistes de
découverte de serveur de noms et la pré-extraction de DNSKEY et en
limitant le nombre de fois qu’un point de délégation peut présenter une
recherche dans le cache pour des enregistrements manquants.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30698">CVE-2022-30698</a>

<p>and <a href="https://security-tracker.debian.org/tracker/CVE-2022-30699">CVE-2022-30699</a>.</p>

<p>Unbound est vulnérable à une nouveau type d’attaque <q>ghost domain names</q>.
La vulnérabilité fonctionne en ciblant une instance Unbound. Unbound est
interrogé pour un nom de domaine véreux quand l’information de délégation de
cache est sur le point d’expirer. Le serveur de noms véreux retarde la réponse
de telle façon que cette information ait expirée. Lors de la réception de la
réponse retardée contenant l’information de délégation, Unbound surcharge les
entrées désormais expirées. Cette action peut être répétée quand l’information
de délégation est sur le point d’expirer, faisant que l’information de
délégation véreuse soit toujours à jour. Désormais, Unbound enregistre
l’instant de démarrage d’une requête et l’utilise pour décider si l’information
de délégation en cache peut être surchargée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28935">CVE-2020-28935</a>

 <p>Unbound contenait une vulnérabilité locale qui pouvait permettre une attaque
locale par lien symbolique.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.9.0-2+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets unbound.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de unbound,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/unbound">\
https://security-tracker.debian.org/tracker/unbound</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3371.data"
# $Id: $
