#use wml::debian::translation-check translation="7636af0cce7aa205c5abc5bd5dbb1236e7bbfd5c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4037">CVE-2021-4037</a>

<p>Christian Brauner a signalé que la fonction inode_init_owner pour le
système de fichiers XFS dans le noyau Linux permet aux utilisateurs locaux
de créer des fichiers avec une appartenance à un groupe non prévue
permettant à des attaquants d'élever leurs privilèges en rendant un simple
fichier exécutable et SGID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0171">CVE-2022-0171</a>

<p>Mingwei Zhang a signalé qu'un problème d'incohérence de cache dans l'API
SEV dans le sous-système KVM peut avoir pour conséquence un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1184">CVE-2022-1184</a>

<p>Un défaut a été découvert dans le pilote du système de fichiers ext4 qui
peut conduire à une utilisation de mémoire après libération. Un utilisateur
local autorisé à monter des systèmes de fichiers arbitraires pouvait
exploiter cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1679">CVE-2022-1679</a>

<p>L'outil syzbot a découvert une situation de compétition dans le pilote
ath9k_htc qui pouvait conduire à une utilisation de mémoire après
libération. Cela pouvait être exploitable pour provoquer un déni de service
(plantage ou corruption de mémoire) ou éventuellement une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2153">CVE-2022-2153</a>

<p><q>kangel</q> a signalé un défaut dans l'implémentation de KVM pour les
processeurs x86 qui pouvait conduire à un déréférencement de pointeur NULL.
Un utilisateur local autorisé à accéder à /dev/kvm pouvait exploiter cela
pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2602">CVE-2022-2602</a>

<p>Une situation de compétition entre le traitement d'une requête io_uring
et le ramasse-miettes d'un socket Unix a été découverte. Un attaquant peut
tirer avantage de ce défaut pour une élévation locale de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2663">CVE-2022-2663</a>

<p>David Leadbeater a signalé des défauts dans le module du protocole de
connexion nf_conntrack_irc. Quand ce module est activé sur un pare-feu, un
utilisateur externe sur le même réseau IRC qu'un utilisateur interne
pouvait exploiter son analyse laxiste pour ouvrir des ports TCP arbitraires
dans le pare-feu, révéler son adresse IP publique ou pour bloquer sa
connexion IRC au pare-feu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2905">CVE-2022-2905</a>

<p>Hsin-Wei Hung a signalé un défaut dans le vérificateur eBPF qui peut
conduire à une lecture hors limites. Si l'utilisation non privilégiée
d'eBPF est activée, cela peut divulguer des informations sensibles. Cette
utilisation était déjà désactivée par défaut, ce qui devait pallier
complètement la vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3028">CVE-2022-3028</a>

<p>Abhishek Shah a signalé une situation de compétition dans le
sous-système AF_KEY qui pouvait conduire à une écriture ou une lecture hors
limites. Un utilisateur local pouvait exploiter cela pour provoquer un déni
de service (plantage ou corruption de mémoire) pour obtenir des
informations sensibles ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3061">CVE-2022-3061</a>

<p>Un défaut a été découvert dans le pilote i740 qui peut avoir pour
conséquence un déni de service.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3176">CVE-2022-3176</a>

<p>Un défaut d'utilisation de mémoire après libération a été découvert dans
le sous-système io_uring qui peut avoir pour conséquence une élévation
locale de privilèges vers ceux du superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3303">CVE-2022-3303</a>

<p>Une situation de compétition dans la fonction snd_pcm_oss_sync dans le
sous-système son dans le noyau Linux, due à un verrouillage incorrect, peut
avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3586">CVE-2022-3586</a> (ZDI-22-1452)

<p>Zero Day Initiative a signalé un défaut dans l'ordonnanceur réseau
sch_sfb, qui pouvait conduire à une utilisation de mémoire après libération
et une fuite d'informations sensibles issues du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3621">CVE-2022-3621</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-3646">CVE-2022-3646</a>

<p>L'outil syzbot a découvert des défauts dans le pilote du système de
fichiers nilfs2 qui peuvent conduire à un déréférencement de pointeur NULL
ou une fuite de mémoire. Un utilisateur autorisé à monter des images
arbitraires de système de fichiers pouvait utiliser cela pour provoquer un
déni de service (plantage ou épuisement de ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3625">CVE-2022-3625</a>

<p>Un défaut a été découvert dans le sous-système devlink qui peut conduire
à une utilisation de mémoire après libération. L'impact de sécurité de ce
défaut n'est pas clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3629">CVE-2022-3629</a>

<p>L'outil syzbot a découvert une fuite de mémoire dans l'implémentation du
protocole Virtual Socket. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (épuisement de ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3633">CVE-2022-3633</a>

<p>Le Linux Verification Center a découvert une fuite de mémoire dans
l'implémentation du protocole SAE J1939. Un utilisateur local pouvait
exploiter cela pour provoquer un déni de service (épuisement de
ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3635">CVE-2022-3635</a>

<p>Plusieurs situations de compétition ont été découvertes dans le pilote
ATM idt77252, qui peuvent conduire à une utilisation de mémoire après
libération si le module est supprimé. L'impact de sécurité de ce défaut
n'est pas clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3649">CVE-2022-3649</a>

<p>L'outil syzbot a découvert des défauts dans le pilote du système de
fichiers nilfs2 qui peuvent conduire à une utilisation de mémoire après
libération.  Un utilisateur autorisé à monter des images arbitraires de
système de fichiers pouvait utiliser cela pour provoquer un déni de service
(plantage ou épuisement de ressources) ou éventuellement pour une élévation
de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-20421">CVE-2022-20421</a>

<p>Une vulnérabilité d'utilisation de mémoire après libération a été
découverte dans la fonction binder_inc_ref_for_node dans le pilote de
création de lien d'Android. Sur les systèmes où le créateur de lien est
chargé, un utilisateur local pouvait exploiter cela pour une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-20422">CVE-2022-20422</a>

<p>Une situation de compétition a été découverte dans l'émulateur
d'instruction pour les systèmes Arm 64 bits. Des modifications concurrentes
des sysctl qui contrôlent l'émulateur pouvaient avoir pour conséquence un
déréférencement de pointeur NULL. L'impact de sécurité de ce défaut n'est
pas clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39188">CVE-2022-39188</a>

<p>Jann Horn a signalé une situation de compétition dans le traitement par
le noyau du <q>démappage</q> de certaines plages de mémoire. Quand un
pilote créait un mappage de mémoire avec l'indicateur VM_PFNMAP, ce que
font de nombreux pilotes GPU, le mappage de mémoire pouvait être supprimé
et libéré avant d'avoir été purgé des TLB du processeur. Cela pouvait avoir
pour conséquence une utilisation de mémoire de page après libération. Un
utilisateur local doté de l'accès à ce type de périphérique pouvait
exploiter cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39190">CVE-2022-39190</a>

<p>Gwangun Jung a signalé un défaut dans le sous-système nf_tables. Un
utilisateur local pouvait exploiter cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39842">CVE-2022-39842</a>

<p>Un dépassement d'entier a été découvert dans le pilote vidéo pxa3xx-gcu
qui pouvait conduire à une écriture de tas hors limites.</p>

<p>Ce pilote n'est pas activé dans les configurations du noyau officiel
de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40307">CVE-2022-40307</a>

<p>Une situation de compétition a été découverte dans le pilote
capsule-loader d'EFI, qui pouvait conduire à une utilisation de mémoire
après libération. Un utilisateur local autorisé à accéder à ce périphérique
(/dev/efi_capsule_loader) pouvait exploiter cela pour provoquer un déni de
service (plantage ou corruption de mémoire) ou éventuellement pour une
élévation de privilèges. Cependant, ce périphérique est normalement
accessible uniquement au superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41222">CVE-2022-41222</a>

<p>Une situation de compétition a été découverte dans le sous-système de
gestion de la mémoire qui peut conduire à des entrées TLB périmées. Un
utilisateur local pouvait exploiter cela pour provoquer un déni de service
(corruption de mémoire ou plantage), une fuite d'informations ou une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41674">CVE-2022-41674</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-42719">CVE-2022-42719</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-42720">CVE-2022-42720</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-42721">CVE-2022-42721</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-42722">CVE-2022-42722</a>

<p>Soenke Huster a découvert plusieurs vulnérabilités dans le sous-système
mac80211, déclenchées par des trames WLAN, qui peuvent avoir pour
conséquence un déni de service ou l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43750">CVE-2022-43750</a>

<p>L'outil syzbot a découvert que le pilote de surveillance du bus USB
(usbmon) permettait à des programmes de l'espace utilisateur d'écraser des
structures de données du pilote. Un utilisateur local autorisé à accéder à
un périphérique de surveillance de bus USB pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une élévation de privilèges. Cependant, par défaut,
seul le superutilisateur peut accéder à ce type de périphérique.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
5.10.149-2~deb10u1. Cette mise à jour corrige également une régression pour
certains vieux PC 32 bits (bogue <a
href="https://bugs.debian.org/1017425">nº 1017425</a>) et active le pilote
i10nm_edac (bogue <a href="https://bug.debian.org/1019248">nº 1019248</a>).
Elle comprend beaucoup plus de corrections de bogues issues des mises à
jour des versions stables de 5.10.137 à 5.10.149 inclus.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-5.10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-5.10, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-5.10">\
https://security-tracker.debian.org/tracker/linux-5.10</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3173.data"
# $Id: $
