#use wml::debian::translation-check translation="8d0f3f513643db311367760f031cb36332721444" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>* <a href="https://security-tracker.debian.org/tracker/CVE-2019-10208">CVE-2019-10208</a> :
« TYPE » dans « pg_temp » exécute du SQL arbitraire lors de l’exécution de « SECURITY DEFINER »</p>

<p>Versions affectées : 9.4 - 11</p>

<p>Étant donné une fonction « SECURITY DEFINER » appropriée, un attaquant peut
exécuter du SQL arbitraire sous l’identité du possesseur de la fonction.
Une attaque requiert la permission « EXECUTE » pour la fonction qui doit elle-même
contenir un appel de fonction avec une correspondance inexacte du type
des arguments. Par exemple, elle est inexacte pour « length('foo'::varchar) » et « length('foo') »
tandis qu'elle est exacte pour « length('foo'::text) ». Dans le cadre de l’exploitation de
cette vulnérabilité, l’attaquant utilise « CREATE DOMAIN » pour créer un type dans
un schéma « pg_temp ». Le schéma d'attaque et son correctif sont similaires à ceux
de <a href="https://security-tracker.debian.org/tracker/CVE-2007-2138">CVE-2007-2138</a>.</p>

<p>L’écriture de fonctions « SECURITY DEFINER » continue d’exiger de suivre les
considérations notées dans la documentation :</p>

<p>https://www.postgresql.org/docs/devel/sql-createfunction.html#SQL-CREATEFUNCTION-SECURITY</p>

<p>Le projet PostgreSQL remercie Tom Lane pour le signalement de ce problème.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 9.4.24-0+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets postgresql-9.4.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1874.data"
# $Id: $
