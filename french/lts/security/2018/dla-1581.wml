#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans uriparser, une bibliothèque
d’analyse d’identifiants normalisés (URI).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19198">CVE-2018-19198</a>

<p>UriQuery.c permet une écriture hors limites à l'aide d'une fonction
uriComposeQuery* ou uriComposeQueryEx* parce que le caractère « &amp; » est mal
géré dans certains contextes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19199">CVE-2018-19199</a>

<p>UriQuery.c permet un dépassement d'entier à l'aide d'une fonction
 uriComposeQuery* ou uriComposeQueryEx* à cause d’une absence de vérification
de multiplication.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19200">CVE-2018-19200</a>

<p>UriCommon.c permet des tentatives d’opérations pour une entrée NULL à l'aide
d'une fonction uriResetUri*.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 0.8.0.1-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets uriparser.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1581.data"
# $Id: $
