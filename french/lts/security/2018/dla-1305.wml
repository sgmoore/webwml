#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ming.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5251">CVE-2018-5251</a>

<p>Vulnérabilité d’erreur d’entier signé (décalage à gauche d’une valeur
négative) dans la fonction readSBits (util/read.c). Des attaquants distants
pourraient exploiter cette vulnérabilité pour provoquer un déni de service
à l'aide d'un fichier SWF contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5294">CVE-2018-5294</a>

<p>Vulnérabilité de dépassement d’entier (causé par à un décalage à gauche hors
intervalle) dans la fonction readUInt32 (util/read.c). Des attaquants distants
pourraient exploiter cette vulnérabilité pour provoquer un déni de service
à l'aide d'un fichier SWF contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6315">CVE-2018-6315</a>

<p>Dépassement d’entier et lecture hors limites résultante dans la fonction
outputSWF_TEXT_RECORD (util/outputscript.c). Des attaquants distants pourraient
exploiter cette vulnérabilité pour provoquer un déni de service ou un autre
impact non précisé à l'aide d'un fichier SWF contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6359">CVE-2018-6359</a>

<p>Vulnérabilité d’utilisation après libération dans la fonction decompileIF
(util/decompile.c). Des attaquants distants pourraient exploiter cette
vulnérabilité pour provoquer un déni de service ou un autre impact non précisé
à l'aide d'un fichier SWF contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.4.4-1.1+deb7u7.</p>


<p>Nous vous recommandons de mettre à jour vos paquets ming.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1305.data"
# $Id: $
