#use wml::debian::translation-check translation="cc2dbf767d14c9f1493590101e5e159f26d0268b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans libxml2, une
bibliothèque fournissant la gestion de la lecture, de la modification et de
l'écriture de fichiers XML et HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40303">CVE-2022-40303</a>

<p>Maddie Stone a découvert que l'absence de vérifications de sécurité dans
plusieurs fonctions peuvent avoir pour conséquence un dépassement d'entier
lors de l'analyse d'un document XML lorsque l'option XML_PARSE_HUGE est
activée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40304">CVE-2022-40304</a>

<p>Ned Williamson et Nathan Wachholz ont découvert une vulnérabilité lors
du traitement de la détection de cycles de références d'entité, qui peut
avoir pour conséquence la corruption d'entrées du dictionnaire. Ce défaut
pouvait conduire à des erreurs de logique, y compris des erreurs de mémoire
comme des défauts de double libération de zone de mémoire.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.9.10+dfsg-6.7+deb11u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxml2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxml2">\
https://security-tracker.debian.org/tracker/libxml2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5271.data"
# $Id: $
