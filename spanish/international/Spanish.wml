#use wml::debian::template title="Debian en español"

# The contents of this page is completely the responsibility of
# the Spanish translation team


<p lang=es>Desde hace muchos años distintos colaboradores
y desarrolladores trabajan en hacer Debian accesible a los usuarios
hispanoparlantes. El resultado de este trabajo lo disfrutan estos
usuarios, gracias al trabajo desinteresado de estos colaboradores
y desarrolladores.</p>

<h1>Localización de Debian al español</h1>

<p lang=es>Si lo que quiere es, directamente, localizar su sistema
Debian para que esté preparado para funcionar en español
tan sólo tiene que seleccionar en la instalación el español como idioma
preferente. De esta forma, el sistema quedará adaptado para esta localización
y se instalarán los programas necesarios para dar soporte al idioma.
</p>

<p>Si no ha hecho esta selección en el proceso de instalación, puede
configurar su sistema en español instalando el paquete <a
href="https://packages.debian.org/tasksel">tasksel</a>
(posiblemente lo tenga ya instalado), ejecutar (como superusuario)
<tt>tasksel</tt> y seleccionando Español (<em>Spanish environment</em>) de
entre las tareas de Localización.
</p>

<p>Una vez hecho ésto se instalarán la mayoría de los paquetes útiles para 
hispanoparlantes y su sistema estará adaptado para usar el español.
</p>

<p>Siga leyendo para encontrar más información de qué le va a
poder ofrecer Debian en cuanto a recursos en español y dónde 
va a poder encontrar más información.</p>

<h1>Internacionalización de Debian al español</h1>

<p>El esfuerzo de internacionalización se dirige desde la lista
<a href="https://lists.debian.org/debian-l10n-spanish/">debian-l10n-spanish</a>.
Si está interesado en <strong>colaborar</strong>, conviene que
consulte antes la <a href="spanish/proyectos">página de proyectos de
traducción en curso</a>. Tambien diríjase a las
<a href="spanish/">páginas de coordinación</a> o discuta
los temas que crea conveniente en la <a
href="mailto:debian-l10n-spanish@lists.debian.org">lista de
traductores</a>.</p>

<p>En el Wiki de Debian, el esfuerzo de internalización dispone de una
<a href="https://wiki.debian.org/es/L10n/Spanish">página específica</a>
que se utilizar para documentar la situación actual y coordinar traducciones.
</p>

<p>Actualmente los usuarios hispanoparlantes ven los frutos de este
esfuerzo a través de varias vías:
</p>

<ul>
<li><a href="#listas">listas de correo</a>.</li>
<li><a href="#doc">traducción de documentación</a>.</li>
<li><a href="#www">traducción del sitio de web</a>.</li>
<li><a href="#pack">paquetes específicos para usuarios hispanoparlanes</a>.</li>
<li><a href="#distrib">distribuciones de Debian en español</a>.</li>
<li><a href="#prog">programas en español</a>.</li>
<li><a href="#developer">desarrolladores hispanoparlantes</a>.</li>
</ul>


<h2><a name="listas"></a>Listas de correo</h2>

<p>La lista de correo para usuarios de habla hispana. Para suscribirse
envíe un mensaje a <a
href="mailto:debian-user-spanish-request@lists.debian.org">debian-user-spanish-request@lists.debian.org</a>
con<br>

<tt>
subscribe &lt;su_dirección_de_correo-e&gt;
</tt>
en el contenido.</p>

<p>Puede usar esta lista para resolver sus dudas relacionadas con
Debian si su conocimiento del inglés no le permite usar
debian-user@lists.debian.org (donde encontrará una audiencia más
grande, aunque quizás también más ruido). En particular podrán
ayudarlo en la adaptación de Debian al español más fácilmente que en
otras listas de Debian.</p>

<p>La lista tiene un cierto tráfico, pero suele haber respuestas para
la mayor parte de los problemas en el mismo día. Los desarrolladores
de Debian hispanoparlantes suelen estar suscritos a la misma.</p>

<h2><a name="doc"></a>Traducción de documentación</h2>

<p>La documentación sobre Debian en español es abundante, aunque no
son muchos los documentos de Debian <q>oficiales</q> 
(los pertenecientes al <a href="$(HOME)/doc">Proyecto de Documentación</a>)
traducidos. Sin embargo, se está preparando el servidor de documentación de 
Debian para poder ofrecer los documentos traducidos de forma transparente.</p>

<p>El coordinador actual de la traducción de la documentación es <a
href="mailto:jfs@debian.org">Javier Fernández-Sanguino Peña</a>.
Este trabajo de traducción, como todos los demás, se coordina a través de la 
lista de correo de traductores al español, 
<a href="https://lists.debian.org/debian-l10n-spanish/">debian-l10n-spanish</a>.
</p>

<p>Si está interesado en hacer una traducción puntual de un documento del
<a href="https://www.debian.org/doc/ddp">Proyecto de Documentación de
Debian</a> puede enviarla a la lista para que se incluya en el
<a href="https://salsa.debian.org/ddp-team">repositorio correspondiente</a>.
Si su trabajo va a extenderse de una forma razonable, le
recomendamos que se suscriba a la lista <a
href="https://lists.debian.org/debian-l10n-spanish/">debian-l10n-spanish</a>
para coordinar sus esfuerzos con el resto del equipo de traducción.</p>

<p>Visite también las <a href="spanish/">páginas de coordinación</a>.</p>

<p>Algunos de los documentos traducidos disponibles los puede encontrar
en paquetes Debian que puede instalar para leerlos localmente. En
concreto, <a
href="https://packages.debian.org/debian-reference-es">debian-reference-es</a>,
la referencia del administrador de Debian.

<p>También se han empaquetado algunos otros documentos traducidos por otros
proyectos de traducción (como los del <a
href="http://es.tldp.org/htmls/pameli.html">proyecto LUCAS</a> o más recientemente
el proyecto multi-idioma y para varias distribuciones 
<a href="https://salsa.debian.org/manpages-l10n-team/manpages-l10n">manpages-l10n</a>) para que los
pueda leer desde su ordenador. En concreto las páginas de manual del sistema
traducidas al español, que podrá utilizar instalando el paquete <a
href="https://packages.debian.org/manpages-es">manpages-es</a>.
</p>

<h2><a name="www"></a>Traducción del sitio web</h2>

<p> La traducción del sitio es un esfuerzo continuo ya que, debido
a la constante inclusión de contenidos, nunca se consigue que la
traducción del sitio sea del 100% (de hecho se pueden
ver las estadísticas exactas <a href="$(HOME)/devel/website/stats/">aquí</a>).
Si desea colaborar en la traducción de este sitio al español,
consulte las <a href="spanish/www">páginas de coordinación</a>.
</p>

<p>Verá en español todas las páginas del sitio web si tiene
adecuadamente configurado su navegador (<a href="$(INTRO)/cn.es.html">aquí
tiene cómo hacerlo</a>).  Si al acceder a contenidos en el sitio web se le muestran
páginas en inglés, posiblemente será debido a que no están aún traducidas
(si lo desea, puede <a href="spanish/www">ayudar</a> fácilmente a que lo
estén).</p>

<p>Todas las páginas informan automáticamente del estado de la traducción
si la versión traducida está desfasada con respecto a la original (generalmente
en inglés). Si tiene dudas de si la versión que está leyendo es la última
disponible, en el pie de cada página tiene un enlace a la correspondiente
versión inglesa de la página.</p>

<p>Este trabajo lo coordina Laura Arjona Reina, utilizando la lista de correo del
grupo de traducción al español. Si necesita ayuda contacte con 
<a href="mailto:debian-l10n-spanish@debian.org">la lista de traductores al español</a>.
</p>

<h2><a name="pack"></a>Paquetes de interés para usuarios hispanoparlantes</h2>

<p>Hay una serie de paquetes de Debian que tienen un especial interés
para los usuarios de Debian de habla hispana. Si usted habla español
le recomendamos que instale los siguientes paquetes:</p>

<ul>

<li>Diccionarios en español:
<a href="https://packages.debian.org/ispanish">ispanish</a>,
<a href="https://packages.debian.org/wspanish">wspanish</a>,
y
<a href="https://packages.debian.org/aspell-es">aspell</a>.</li>

<li>Traducción y localización de diversos paquetes al español: 
<a href="https://packages.debian.org/search?keywords=l10n-es">https://packages.debian.org/search?keywords=l10n-es</a>,</li>

<li><a href="https://packages.debian.org/fortunes-es">fortunes-es</a>, citas
en español.</li>

</ul>

<p>Si desea ser desarrollador de Debian instale también:</p>
<ul>
<li><a href="https://packages.debian.org/maint-guide-es">maint-guide-es</a>,
la guía del desarrollador de Debian en castellano</li>
</ul>

<p>En Debian existe un perfil para usuarios hispanoparlantes,
de forma que seleccionando éste se instalen muchos de estos paquetes. El
perfil se llama <a
href="https://packages.debian.org/task-spanish">task-spanish</a>. Este
perfil está integrado dentro del programa <tt>tasksel</tt>
de Debian y se selecciona automáticamente cuando se elige
el español como idioma durante la instalación.</p>

<h2><a name="distrib"></a>Distribuciones de Debian en español</h2>

<p> Nota: <a href="https://web.archive.org/web/20050326103207/http://www.id-agora.com/citius.shtml">Citius Debian</a>
fue la primera distribución con
la instalación traducida por completo al español (de hecho la habían
creado desarrolladores hispanos de Debian) y también incluía un manual de 
Debian impreso en español (el 
<a href="$(HOME)/releases/potato/installmanual">oficial de Debian 2.2</a>).  
Además, en los CD's se podía encontrar toda la documentación disponible en
español de los proyectos externos a Debian que traducen y generan
documentación en este idioma.
</p>

<p>Actualmente Debian ya incorpora traducción al español en gran parte del sistema,
aspecto que heredan también sus <a href="/derivatives">distribuciones derivadas</a>.
Algunas de ellas puede que ofrezcan una mayor adaptación a sus requisitos.</p>

<h2><a name="prog"></a>Programas en español</h2>

<p>El equipo de traducción no se dedica, de momento, a traducir
programas en Debian al español si estos no han sido desarrollados
para Debian. Algunos programas, como <tt>dpkg</tt>, <tt>apt</tt> o 
<tt>dselect</tt> sí han sido internacionalizados dentro de Debian.</p>

<p>Mire <a href="l10n/po/es">en el monitor de traducciones de Debian</a>
aquellos paquetes que han sido traducidos dentro de Debian.</p>

<p>Así que si su programa favorito no está internacionalizado
(compruebe antes los valores de las variables de entorno LANG, LC_ALL
y LC_MESSAGES) en Debian es posible que aún no se haya
internacionalizado la versión original. Puede considerar el
colaborar en internacionalizarlo: mire la herramienta GNU <a
href="https://www.gnu.org/software/gettext/gettext.html">gettext</a>
para información sobre cómo internacionalizar programas de una forma
sencilla y estándar.</p>

<p>Puede contribuir sus trabajos a través del <a
href="https://translationproject.org/">Proyecto de
Traduciones Libres</a> (<em>Free Translation Project</em>, n. del a.).
Si desea ver el estado de las traducciones al español existe una 
<a href="https://translationproject.org/team/es.html">base
de datos</a> de traducciones y traductores.</p>

<h2><a name="developer"></a>Desarrolladores de Debian hispanoparlantes</h2>

<p>Existe un buen número de desarrolladores hispanoparlantes,
distribuidos en distintos países en Europa y América. Si tiene
interés en contactar con los desarrolladores se le recomienda
contactar con ellos a través de la <a href="mailto:debian-devel-spanish@lists.debian.org">lista de desarrolladores (en español)</a>.</p>

<p><a href="spanish/desarrolladores">Aquí</a> encontrará
información detallada de los desarrolladores por país.</p>
