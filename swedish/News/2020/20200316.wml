#use wml::debian::translation-check translation="c1e997409ebbfee3f612efc60c0f1e3fe81c7e9c"
<define-tag pagetitle>Debians officiella kommunikationskanaler</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news

<p>
Då och då får vi frågor i Debian om våra officiella kommunikationskanaler
och frågor om statusen för Debian om vilka som kan äga liknande namngivna
webbplatser.
</p>

<p>
Debians huvudwebbplats <a href="https://www.debian.org">www.debian.org</a>
är vårt primära kommunikationsmedium. De som söker information om
aktuella evenemang och utvecklingsframsteg i gemenskapen kan vara intresserade
av <a href="https://www.debian.org/News/">Debiannyheter</a>-sektionen av
Debians webbplats.

För mindre formella tillkännagivanden har vi den officiella bloggen
<a href="https://bits.debian.org">Bits from Debian</a>, samt
tjänsten <a href="https://micronews.debian.org">Debian micronews</a> för
kortare nyheter.
</p>

<p>
Vårt officiella nyhetsbrev
<a href="https://www.debian.org/News/weekly/">Debians projektnyheter</a>
och alla våra officiella tillkännagivanden av nyheter eller projektförändringar
postas både på vår webbsida och vår officiella sändlista
<a href="https://lists.debian.org/debian-announce/">debian-announce</a> eller
<a href="https://lists.debian.org/debian-news/">debian-news</a>.
Publicering till dessa sändlistor är begränsat.
</p>

<p>
Vi vill även ta denna möjlighet att tillkännage hur Debianprojektet,
eller förkortat Debian, är strukturerat.
</p>

<p>
Debian har en struktur som regleras av våra
<a href="https://www.debian.org/devel/constitution">stadgar</a>.
Tjänstemän och delegerade medlemmar listas på vår sida för vår
<a href="https://www.debian.org/intro/organization">organisationsstruktur</a>.
Ytterligare grupper listas på vår sida för <a href="https://wiki.debian.org/Teams">grupper</a>.
</p>

<p>
Den fullständiga listan på officiella Debianmedlemmar kan hittas på vår
sida
<a href="https://nm.debian.org/members">Nya medlemmar</a>,
där våra medlemskap hanteras. En mer fullständig lista på bidragslämnare till
Debian kan hittas på vår sida
<a href="https://contributors.debian.org">Bidragslämnare</a>.
</p>

<p>
Om du har frågor kan du (på engelska) nå pressgruppen på
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

<h2>Om Debian</h2>
<p>Debianprojektet är en sammanslutning av utvecklare av fri mjukvara som
ger frivilligt av sin tid och insats för att producera det helt fria
operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>
<p>För ytterligare information, vänligen besök Debians webbsidor på
<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post (på engelska)
till &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
