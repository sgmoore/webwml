#use wml::debian::template title="Suporte a usuários(as)"
#use wml::debian::toc
#use wml::debian::translation-check translation="c7a64739b495b3bd084b972d820ef78fc30a3f8a"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#irc">IRC (suporte em tempo real)</a></li>
  <li><a href="#mail_lists">Listas de discussão</a></li>
  <li><a href="#usenet">Grupos de notícias da Usenet</a></li>
  <li><a href="#forums">Fórum de usuários(as) Debian</a></li>
  <li><a href="#maintainers">Como entrar em contato com mantenedores(as) de pacotes</a></li>
  <li><a href="#bts">Sistema de acompanhamento de bugs</a></li>
  <li><a href="#release">Problemas conhecidos</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> O suporte ao Debian é
oferecido por um grupo de voluntários(as). Se este suporte oferecido pela
comunidade não atender às suas necessidades e você não encontrar a resposta em
nossa <a href="doc/">documentação</a>, você pode contatar um(a)
<a href="consultants/">consultor(a)</a> para responder suas dúvidas, ou manter
ou adicionar funcionalidades adicionais ao seu sistema Debian.</p>
</aside>

<h2><a id="irc">IRC (suporte em tempo real)</a></h2>

<p>
O <a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) é uma ótima
maneira de conversar com pessoas do mundo todo em tempo real. É um sistema
de bate-papo baseado em texto para mensagens instantâneas. No IRC, você pode
entrar em salas de bate-papo (os chamados canais) ou conversar diretamente com
pessoas individualmente por meio de mensagens privadas.
</p>

<p>
Canais de IRC dedicados ao Debian podem ser encontrados no
<a href="https://www.oftc.net/">OFTC</a>. Para uma lista completa de canais
Debian, por favor consulte nossa
<a href="https://wiki.debian.org/IRC">Wiki</a>. Você também pode usar um
<a href="https://netsplit.de/channels/index.en.php?net=oftc&amp;chat=debian">motor de busca</a>
para achar canais relacionados ao Debian.
</p>

<h3>Clientes de IRC</h3>

<p>
Para se conectar à rede IRC, você pode usar o
<a href="https://www.oftc.net/WebChat/">WebChat</a> do OFTC em seu navegador web
preferido ou instalar um cliente em seu computador. Existem muitos clientes
diferentes por aí, alguns com interface gráfica, outros para o console. Alguns
clientes de IRC populares foram empacotados para o Debian, por exemplo:
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/ircii">ircII (modo texto)</a>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a> (modo texto)</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat (GTK)</a>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a> (KDE)</li>
</ul>

<p>
A wiki Debian oferece uma
<a href="https://wiki.debian.org/IrcClients">lista de clientes de IRC</a>
mais abrangente que estão disponíveis como pacotes Debian.
</p>

<p>
Depois de instalar o cliente, você precisa dizer a ele para se conectar ao
servidor. Na maioria dos clientes, você pode fazer isso digitando:
</p>

<pre>
/server irc.debian.org
</pre>

<p>
O hostname irc.debian.org é um alias para irc.oftc.net. Em alguns clientes
(como o irssi), você precisará digitar isto em vez do anterior:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<h3>Entre em um canal</h3>

<p>
Assim que estiver conectado, entre no canal <code>#debian</code> digitando
esse comando:
</p>

<pre>
/join #debian
</pre>

<p>Ou, se preferir, ingresse no canal <code>#debian-br</code> para conversar
em português:</p>

<pre>
/join #debian-br
</pre>

<p>
Nota: clientes gráficos como o HexChat ou o Konversation frequentemente tem um
botão ou uma entrada no menu para conectar nos servidores e entrar em canais.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/DebianIRC">Leia nossa FAQ sobre IRC</a></button></p>

<h2><a id="mail_lists">Listas de discussão</a></h2>

<p>
Mais de mil <a href="intro/people.en.html#devcont">desenvolvedores(as)</a>
ativos(as) espalhados pelo mundo trabalham no Debian em seu tempo livre — e em
seus próprios fusos horários. Portanto, nos comunicamos principalmente por
e-mail. Da mesma forma, a maior parte das conversas entre desenvolvedores(as) e
usuários(as) do Debian acontece em diferentes
<a href="MailingLists/">listas de discussão</a>:
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<ul>
   <li>Para suporte aos(às) usuários(as) em português, entre em contato com a
lista de discussão
<a href="https://lists.debian.org/debian-user-portuguese/">debian-user-portuguese</a>.</li>
   <li>Para suporte aos(às) usuários(as) em inglês, entre em contato com a lista
de discussão <a href="https://lists.debian.org/debian-user/">debian-user</a>.</li>
   <li>Para suporte aos(às) usuários(as) em outros idiomas, verifique o
<a href="https://lists.debian.org/users.html">índice</a> de outras listas de
discussão de usuários.</li>
</ul>

<p>
Você pode navegar em nosso
<a href="https://lists.debian.org/">arquivo das listas de discussão</a> ou
<a href="https://lists.debian.org/search.html">pesquisar </a> os arquivos sem
necessidade de inscrição.
</p>

<p>
Claro, existem muitas outras listas de discussão dedicadas a outros aspectos do
vasto ecossistema do Linux que não são específicos do Debian. Por favor use
seu buscador favorito para encontrar a lista mais adequada para seu propósito.
</p>

<h2><a id="usenet">Grupos de notícias da Usenet</a></h2>

<p>
Muitas de nossas <a href="#mail_lists">listas de discussão</a> podem ser
consultadas como grupos de notícias na hierarquia <kbd>linux.debian.*</kbd>.
</p>

<h2><a id="forums">Fórum de usuários(as) Debian</h2>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p>
O <a href="https://forums.debian.net">fórum de usuários(as) Debian</a> é um
portal web onde milhares de usuários(as) discutem tópicos
relacionados ao Debian, fazem perguntas e ajudam uns aos(às) outros(as)
com respostas. Você pode ler todas as discussões sem ter que se registrar. Se
quiser participar da discussão e publicar suas próprias postagens, por
favor registre-se e faça o login.
</p>

<h2><a id="maintainers">Como entrar em contato com mantenedores(as) de pacotes</a></h2>

<p>
Basicamente, existem duas maneiras comuns de entrar em contato com
mantenedores(as) de um pacote Debian:
</p>

<ul>
   <li>Se quiser relatar um bug, basta enviar um
<a href="Bugs/Reporting">relatório de bug</a>; o(a) mantenedor(a) recebe automaticamente
uma cópia do seu relatório de bug.</li>
   <li>Se quiser simplesmente enviar um e-mail para o(a) mantenedor(a),
use os aliases de e-mail especiais configurados para cada pacote:<br>
       &lt;<em>nome_do_pacote</em>&gt;@packages.debian.org</li>
</ul>

<h2><a id="bts">Sistema de acompanhamento de bugs</a></h2>

<p>
A distribuição Debian tem seu próprio <a href="Bugs/">acompanhador de bugs</a>
com bugs relatados por usuários(as) e desenvolvedores(as). Cada bug tem um
número único e é mantido em arquivo até que seja marcado como resolvido.
Existem duas maneiras diferentes de relatar um bug:
</p>

<ul>
   <li>A forma recomendada é usar o pacote Debian <em>reportbug</em>.</li>
   <li>Como alternativa, você pode enviar um e-mail conforme descrito nesta
<a href="Bugs/Reporting">página</a>.</li>
</ul>

<toc-add-entry name="release" href="releases/stable/">Problemas conhecidos</toc-add-entry>

<p>
Limitações e diversos problemas da versão estável (stable) atual (caso haja)
são descritos nas <a href="releases/stable/">páginas de lançamento</a>.
</p>

<p>
Por favor preste atenção em particular às
<a href="releases/stable/releasenotes">notas de lançamento</a> e
à <a href="releases/stable/errata">errata</a>.
</p>
