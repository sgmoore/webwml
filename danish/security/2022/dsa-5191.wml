#use wml::debian::translation-check translation="fa7bd3e4644f60a727dbe8b56ea07b03f7ff7fc2" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til 
rettighedsforøgelse, lammelsesangreb eller informationslækager:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33655">CVE-2021-33655</a>

    <p>En bruger med adgang til en framebuffer-console-driver, kunne forårsage 
    en hukommelsesskrivning udenfor grænserne via ioctl'en 
    FBIOPUT_VSCREENINFO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2318">CVE-2022-2318</a>

    <p>En anvendelse efter frigivelse i understøttelsen af Amateur Radio X.25 
    PLP (Rose), kunne medføre lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26365">CVE-2022-26365</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-33740">CVE-2022-33740</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-33741">CVE-2022-33741</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-33742">CVE-2022-33742</a>

    <p>Roger Pau Monne opdagede at Xens block- og network-PV-enhedsfrontends, 
    ikke nulstillede hukommelsesregioner før de blev delt med backend'en, 
    hvilket kunne medføre informationsafsløring.  Desuden blev der opdaget at 
    granttabellens granularitet ikke tillod deling af mindre end en 4k-side, 
    hvilket også kunne medføre informationsafsløring.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33743">CVE-2022-33743</a>

    <p>Jan Beulich opdagede at ukorrekt hukommelseshåndtering i Xens 
    netværksbackend, kunne føre til lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33744">CVE-2022-33744</a>

    <p>Oleksandr Tyshchenko opdagede at ARM Xen-gæster kunne forårsage et 
    lammelsesangreb mod Dom0 gennem paravirtuelle enheder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34918">CVE-2022-34918</a>

    <p>Arthur Mongodin opdagede et heapbufferoverløb i undersystemet Netfilter, 
    hvilket kunne medføre lokal rettighedsforøgelse.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 5.10.127-2.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5191.data"
