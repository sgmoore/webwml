#use wml::debian::template title="Система за следене на грешките в Дебиан" BARETITLE=true NOCOPYRIGHT=true
#use wml::debian::translation-check translation="88e6fda5d311846e8cc50c89c3f22aa4a3fa312a"
#include "$(ENGLISHDIR)/Bugs/pkgreport-opts.inc"
#{#style#:<link rel="stylesheet" href="https://bugs.debian.org/css/bugs.css" type="text/css">:##}
{#meta#:
<script type="text/javascript" src="hashbug_redirect.js"></script>
:#meta#}

<p>Дебиан използва система за следене на проблемите, в която се съхраняват всички
подробности за докладваните грешки. На всеки доклад се дава номер и докладът се
държи отворен докато докато проблемът не се отстрани.</p>

<h2>Как да докладваме за открита грешка в Дебиан</h2>

<p>Отделна страница с инструкции и съвети е посветена на това <a
href="Reporting">как да докладваме за открити грешки</a> в Дебиан.</p>

<h2>Документация на системата за следене на грешки</h2>

<ul>
  <li><a href="Developer">Информация за използване на системата за напреднали</a></li>
  <li><a href="server-control">Информация за управление на докладите чрез електронна поща</a></li>
  <li><a href="server-refcard">Кратък справочник на пощенския сървър</a></li>
  <li><a href="Access">Достъп до архива на докладите за грешки</a></li>
  <li><a href="server-request">Заявяване на доклади по email</a></li>  
</ul>

<h2>Разглеждане на доклади за грешки чрез браузър</h2>

<p style="text-align:center">
<img
src="https://qa.debian.org/data/bts/graphs/all.png?m=0.8&amp;h=250&amp;w=600"
alt="Графика на броя доклади" />
</p>

<p>Търсене на доклад по <strong>номер</strong>:
  <br />
  <a name="bugreport"></a>
  <form method="get" action="https://bugs.debian.org/cgi-bin/bugreport.cgi">
  <p>
  <input type="text" size="9" name="bug" value="">
  <label><input type="checkbox" name="mbox" value="yes"> като mbox</label>
  <label><input type="checkbox" name="trim" value="no"> показване на всички заглавки</label>
  <label><input type="checkbox" name="boring" value="yes"> показване на скучните съобщения</label>
  <input type="submit" value="Търсене">
  </p>
  </form>

<h2>Селекция на доклади за грешки чрез браузър</h2>
<a name="pkgreport"></a>

<bts_main_form>

<table class="forms">

<tr><td><h2>Селекция на доклади</h2>
</td>
<td>
<bts_select_form>
</td>
<td>
<p>Допълнителни критерии могат да бъдат добавени и по-късно. Критериите на
едно и също поле се обединяват с операция „логическо или“, а тези на различни полета
– с „логическо и“.</p>
<p>Поддържаните нива на критичност са <bts_severities_all>.</p>
<p>Поддържаните етикети са <bts_tags>.</p>
</td>
</tr>

<tr><td><h2>Включване на доклади</h2></td>
<td>
<bts_include_form>
</td>
<td>
</td>
</tr>

<tr><td><h2>Изключване на доклади</h2></td>
<td>
<bts_exclude_form>
</td>
<td></td>
</tr>

<tr><td><h2>Подреждане</h2></td>
<td>
<bts_orderby_form>
</td>
<td></td>
</tr>

<tr><td><h2>Допълнителни настройки</h2></td>
<td>
<bts_miscopts_form>
</td>
</tr>

<tr><td><h2>Търсене</h2></td><td colspan="2">
<input type="submit" name="submit" value="Търсене">
</td></tr>

</table>
</form>

<p>Горните запитвания могат да бъдат направени и с директно посещение на
съответния адрес:</p>
<ul>
  <li><tt>https://bugs.debian.org/<var>номер</var></tt></li>
  <li><tt>https://bugs.debian.org/mbox:<var>номер</var></tt></li>
  <li><tt>https://bugs.debian.org/<var>пакет</var></tt></li>
  <li><tt>https://bugs.debian.org/src:<var>пакет-източник</var></tt></li>
  <li><tt>https://bugs.debian.org/<var>отговорник@email.адрес</var></tt></li>
  <li><tt>https://bugs.debian.org/from:<var>подател@email.адрес</var></tt></li>
  <li><tt>https://bugs.debian.org/severity:<var>критичност</var></tt></li>
  <li><tt>https://bugs.debian.org/tag:<var>етикет</var></tt></li>
</ul>

<h2>Търсене в докладите за грешки</h2>

## Link to bugs-search.d.o removed because of Bug#629645 (service down):
# <p>В докладите за грешки може да се търси и с нашата <a
# href="https://bugs.debian.org/cgi-bin/search.cgi">търсачка, базирана на
# HyperEstraier.</a></p>

<p>Универсалната база данни за Дебиан (UDD - The Ultimate Debian Database)
предлага
<a href="https://udd.debian.org/bugs/">търсачка за докладите за проблеми</a>.</p>

<p>Друг начин за търсене в докладите за грешки е с използване на
<a href="https://groups.google.com/d/forum/linux.debian.bugs.dist">Google Groups</a>.
</p>

<p>Алтернативн сайт за търсене в докладите за грешки е например
<a href="https://www.mail-archive.com/debian-bugs-dist%40lists.debian.org/">The
Mail Archive</a>.</p>

<h2>Допълнителна информация</h2>

<p>Списък на <a href="https://bugs.debian.org/release-critical/">
Критичните грешки в изданията</a>.</p>

<p>Актуален списък на <a href="pseudo-packages">псевдо-пакети</a>,
поддържан от системата за следене на грешките</p>

<p>Достъпни са следните списъци с доклади:</p>

<ul>
  <li><a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=pkg">Отворени</a>
      и
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=pkg&amp;archive=yes">архивирани</a> по пакети.</li>
  <li><a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=src">Отворени</a>
      и
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=src&amp;archive=yes">архивирани</a>
      по пакети-източници.</li>
  <li><a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=maint">Отворени</a>
      и
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=maint&amp;archive=yes">архивирани</a>
      по отговорници.</li>
  <li><a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=submitter">Отворени</a>
      и
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=submitter&amp;archive=yes">архивирани</a>
      по податели.</li>
</ul>

<p><strong>Забележка:</strong> някои от списъците с доклади за грешки вече
не са на разположение поради проблеми с програмата, която ги генерира.
Извиняваме се за неудобството.</p>

<h2>Съобщаване за нежелана поща</h2>

<p>Системата за следене на грешките често е обект на нежалана поща. Можете да
съобщите за такава като откриете доклада <a href="#bugreport">по номер</a>
и последвате връзката в края на страницата.
</p>

#include "$(ENGLISHDIR)/Bugs/footer.inc"
