<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in coTURN, a TURN and STUN server for
VoIP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4056">CVE-2018-4056</a>

    <p>An SQL injection vulnerability was discovered in the coTURN administrator
    web portal. As the administration web interface is shared with the
    production, it is unfortunately not possible to easily filter outside
    access and this security update completely disable the web interface. Users
    should use the local, command line interface instead.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>

    <p>Default configuration enables unsafe loopback forwarding. A remote attacker
    with access to the TURN interface can use this vulnerability to gain access
    to services that should be local only.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4059">CVE-2018-4059</a>

    <p>Default configuration uses an empty password for the local command line
    administration interface. An attacker with access to the local console
    (either a local attacker or a remote attacker taking advantage of
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>)
    could escalade privileges to administrator of the coTURN
    server.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 4.5.0.5-1+deb9u1.</p>

<p>We recommend that you upgrade your coturn packages.</p>

<p>For the detailed security status of coturn please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/coturn">\
https://security-tracker.debian.org/tracker/coturn</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4373.data"
# $Id: $
