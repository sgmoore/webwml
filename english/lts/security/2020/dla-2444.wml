<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The ppp de-capsulator in tcpdump 4.9.3 can be convinced to allocate
a large amount of memory.</p>

<p>The buffer should be big enough to hold the captured data, but it
doesn’t need to be big enough to hold the entire on-the-network
packet, if we haven’t captured all of it.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.9.3-1~deb9u2.</p>

<p>We recommend that you upgrade your tcpdump packages.</p>

<p>For the detailed security status of tcpdump please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tcpdump">https://security-tracker.debian.org/tracker/tcpdump</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2444.data"
# $Id: $
