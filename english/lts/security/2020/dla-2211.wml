<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an XML external entity vulnerability in
<tt>log4net</tt>, a logging API for the ECMA Common Language Infrastructure
(CLI), sometimes referred to as "Mono".</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1285">CVE-2018-1285</a>

    <p>Apache log4net before 2.0.8 does not disable XML external entities when parsing log4net configuration files. This could allow for XXE-based attacks in applications that accept arbitrary configuration files from users.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.2.10+dfsg-6+deb8u1.</p>

<p>We recommend that you upgrade your log4net packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2211.data"
# $Id: $
