<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a path-traversal issue in Apache Shiro, a
security framework for the Java programming language. A specially-crafted
request could cause an authentication bypass.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1957">CVE-2020-1957</a>

    <p>Apache Shiro before 1.5.2, when using Apache Shiro with Spring dynamic controllers, a specially crafted request may cause an authentication bypass.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.2.3-1+deb8u1.</p>

<p>We recommend that you upgrade your shiro packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2181.data"
# $Id: $
