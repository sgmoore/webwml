<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in php5, a server-side,
HTML-embedded scripting language. An attacker could cause denial of
service (DoS), memory corruption and potentially execution of
arbitrary code, and server-side request forgery (SSRF) bypass.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18218">CVE-2019-18218</a>

    <p>fileinfo: cdf_read_property_info in cdf.c does not restrict the
    number of CDF_VECTOR elements, which allows a heap-based buffer
    overflow (4-byte out-of-bounds write).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7071">CVE-2020-7071</a>

    <p>When validating URL with functions like filter_var($url,
    FILTER_VALIDATE_URL), PHP will accept an URL with invalid password
    as valid URL. This may lead to functions that rely on URL being
    valid to mis-parse the URL and produce wrong data as components of
    the URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21702">CVE-2021-21702</a>

    <p>When using SOAP extension to connect to a SOAP server, a malicious
    SOAP server could return malformed XML data as a response that
    would cause PHP to access a null pointer and thus cause a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21704">CVE-2021-21704</a>

    <p>Multiple firebird issues.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21705">CVE-2021-21705</a>

    <p>SSRF bypass in FILTER_VALIDATE_URL.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
7.0.33-0+deb9u11.</p>

<p>We recommend that you upgrade your php7.0 packages.</p>

<p>For the detailed security status of php7.0 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php7.0">https://security-tracker.debian.org/tracker/php7.0</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2708.data"
# $Id: $
