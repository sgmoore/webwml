<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Mutt, a text-based mailreader supporting MIME,
GPG, PGP and threading, incorrectly handled certain input. An attacker
could possibly use this issue to cause a crash, or expose sensitive
information.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.7.2-1+deb9u6.</p>

<p>We recommend that you upgrade your mutt packages.</p>

<p>For the detailed security status of mutt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mutt">https://security-tracker.debian.org/tracker/mutt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2999.data"
# $Id: $
