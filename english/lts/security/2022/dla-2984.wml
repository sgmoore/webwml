<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jaime Frey discovered a flaw in HTCondor, a distributed workload management
system. An attacker need only have READ-level authorization to a vulnerable
daemon using the CLAIMTOBE authentication method. This means they are able to
run tools like condor_q or condor_status. Many pools do not restrict who can
issue READ-level commands, and CLAIMTOBE is allowed for READ-level commands in
the default configuration. Thus, it is likely that an attacker could execute
this command remotely from an untrusted network, unless prevented by a firewall
or other network-level access controls.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
8.4.11~dfsg.1-1+deb9u2.</p>

<p>We recommend that you upgrade your condor packages.</p>

<p>For the detailed security status of condor please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/condor">https://security-tracker.debian.org/tracker/condor</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2984.data"
# $Id: $
