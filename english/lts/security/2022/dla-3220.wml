<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>ClamAV, an anti-virus utility for Unix, v0.103.7 is a critical patch
release with the following fixes:</p>

<p>* Fix logical signature <q>Intermediates</q> feature.
* Relax constraints on slightly malformed zip archives that contain
  overlapping file entries.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.103.7+dfsg-0+deb10u1.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>For the detailed security status of clamav please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/clamav">https://security-tracker.debian.org/tracker/clamav</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3220.data"
# $Id: $
