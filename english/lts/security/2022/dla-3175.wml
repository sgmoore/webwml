<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Nicky Mouha discovered a buffer overflow in <q>_sha3</q>, the SHA-3 hashing function
module used by <q>hashlib</q> in Python 3.7.</p>

<p>While the attacks require a large volume of data, they could potentially result
in remote code execution.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.7.3-2+deb10u4.</p>

<p>We recommend that you upgrade your python3.7 packages.</p>

<p>For the detailed security status of python3.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python3.7">https://security-tracker.debian.org/tracker/python3.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3175.data"
# $Id: $
