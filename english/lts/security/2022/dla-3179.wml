<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential out-of-bounds write
vulnerability in pixman, a pixel-manipulation library used in many Linux
graphical applications.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-44638">CVE-2022-44638</a>

    <p>In libpixman in Pixman before 0.42.2, there is an out-of-bounds write
    (aka heap-based buffer overflow) in rasterize_edges_8 due to an integer
    overflow in pixman_sample_floor_y.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
0.36.0-1+deb10u1.</p>

<p>We recommend that you upgrade your pixman packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3179.data"
# $Id: $
