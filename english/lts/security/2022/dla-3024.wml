<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential SQL injection vulnerability in
the Django web development framework.</p>

<p>Untrusted data was used as a tolerance parameter in GIS functions and
aggregates when using the Oracle database backend. By passing a suitably
crafted tolerance to GIS functions and aggregates on Oracle, it was potentially
possible to break escaping and inject malicious SQL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9402">CVE-2020-9402</a>
	<p>Django 1.11 before 1.11.29, 2.2 before 2.2.11, and 3.0 before 3.0.4
	allows SQL Injection if untrusted data is used as a tolerance parameter
	in GIS functions and aggregates on Oracle. By passing a suitably
	crafted tolerance to GIS functions and aggregates on Oracle, it was
	possible to break escaping and inject malicious SQL.</p></li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:1.10.7-2+deb9u17.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3024.data"
# $Id: $
