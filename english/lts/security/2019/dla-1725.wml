<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Trail of Bits used the automated vulnerability discovery tools developed
for the DARPA Cyber Grand Challenge to audit zlib. As rsync, a fast,
versatile, remote (and local) file-copying tool, uses an embedded copy of
zlib, those issues are also present in rsync.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9840">CVE-2016-9840</a>

      <p>In order to avoid undefined behavior, remove offset pointer
      optimization, as this is not compliant with the C standard.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9841">CVE-2016-9841</a>

      <p>Only use post-increment to be compliant with the C standard.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9842">CVE-2016-9842</a>

      <p>In order to avoid undefined behavior, do not shift negative values,
      as this is not compliant with the C standard.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9843">CVE-2016-9843</a>

      <p>In order to avoid undefined behavior, do not pre-decrement a pointer
      in big-endian CRC calculation, as this is not compliant with the
      C standard.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5764">CVE-2018-5764</a>

      <p>Prevent remote attackers from being able to bypass the
      argument-sanitization protection mechanism by ignoring --protect-args
      when already sent by client.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.1.1-3+deb8u2.</p>

<p>We recommend that you upgrade your rsync packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1725.data"
# $Id: $
