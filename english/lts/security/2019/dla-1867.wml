<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in WPA supplicant / hostapd. Some
of them could only partially be mitigated, please read below for details.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9495">CVE-2019-9495</a>

    <p>Cache-based side-channel attack against the EAP-pwd implementation:
    an attacker able to run unprivileged code on the target machine
    (including for example javascript code in a browser on a smartphone)
    during the handshake could deduce enough information to discover the
    password in a dictionary attack.</p>

    <p>This issue has only very partially been mitigated against by reducing
    measurable timing differences during private key operations. More
    work is required to fully mitigate this vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9497">CVE-2019-9497</a>

    <p>Reflection attack against EAP-pwd server implementation: a lack of
    validation of received scalar and elements value in the
    EAP-pwd-Commit messages could have resulted in attacks that would
    have been able to complete EAP-pwd authentication exchange without
    the attacker having to know the password. This did not result in the
    attacker being able to derive the session key, complete the following
    key exchange and access the network.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9498">CVE-2019-9498</a>

    <p>EAP-pwd server missing commit validation for scalar/element: hostapd
    didn't validate values received in the EAP-pwd-Commit message, so an
    attacker could have used a specially crafted commit message to
    manipulate the exchange in order for hostapd to derive a session key
    from a limited set of possible values. This could have resulted in an
    attacker being able to complete authentication and gain access to the
    network.</p>

    <p>This issue could only partially be mitigated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9499">CVE-2019-9499</a>

    <p>EAP-pwd peer missing commit validation for scalar/element:
    wpa_supplicant didn't validate values received in the EAP-pwd-Commit
    message, so an attacker could have used a specially crafted commit
    message to manipulate the exchange in order for wpa_supplicant to
    derive a session key from a limited set of possible values. This
    could have resulted in an attacker being able to complete
    authentication and operate as a rogue AP.</p>

    <p>This issue could only partially be mitigated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11555">CVE-2019-11555</a>

    <p>The EAP-pwd implementation did't properly validate fragmentation
    reassembly state when receiving an unexpected fragment. This could
    have lead to a process crash due to a NULL pointer derefrence.</p>

    <p>An attacker in radio range of a station or access point with EAP-pwd
    support could cause a crash of the relevant process (wpa_supplicant
    or hostapd), ensuring a denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.3-1+deb8u8.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1867.data"
# $Id: $
