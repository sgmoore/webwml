<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An information disclosure vulnerability was discovered in Bouncy
Castle, a Java library which consists of various cryptographic
algorithms. The Galois/Counter mode (GCM) implementation was missing a
boundary check that could enable a local application to gain access to
user's private information.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.44+dfsg-3.1+deb7u2.</p>

<p>We recommend that you upgrade your bouncycastle packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-893.data"
# $Id: $
