<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in the Graphite font rendering
engine which might result in denial of service or the execution of
arbitrary code if a malformed font file is processed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.10-1~deb7u1.</p>

<p>We recommend that you upgrade your graphite2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1013.data"
# $Id: $
