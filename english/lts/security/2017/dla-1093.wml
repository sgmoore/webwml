<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in the Tag Image File
Format (TIFF) library and its associated tools.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11335">CVE-2017-11335</a>

    <p>A heap based buffer overflow via a PlanarConfig=Contig image, which
    causes an out-of-bounds write (related to the ZIPDecode function). A
    crafted input may lead to a remote denial of service attack or an
    arbitrary code execution attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12944">CVE-2017-12944</a>

    <p>A mishandling of memory allocation for short files allows attackers
    to cause a denial of service (allocation failure and application
    crash) during a tiff2pdf invocation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13726">CVE-2017-13726</a>

    <p>A reachable assertion abort allows a crafted input to lead to a
    remote denial of service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13727">CVE-2017-13727</a>

    <p>A reachable assertion abort allows a crafted input to lead to a
    remote denial of service attack.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u16.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1093.data"
# $Id: $
