<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15588">CVE-2017-15588</a>

    <p>Jann Horn discovered a race condition that can cause a stale TLB
    entry which might result in privilege escalation, denial of
    service or information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15589">CVE-2017-15589</a>

    <p>Roger Pau Monné discovered a hypervisor stack leak in the x86 I/O
    intercept code, resulting in information leaks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15592">CVE-2017-15592</a>

    <p>Andrew Cooper discovered that incorrect handling of self-linear
    shadow mappings for translated guests might result in denial
    of service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15593">CVE-2017-15593</a>

    <p>Jan Beulich discovered that page type reference counts are
    mishandled which may result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15595">CVE-2017-15595</a>

    <p>Jann Horn discovered that crafted page-table stacking might result
    in denial of service, privilege escalation or information leaks.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.lts1-10.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1181.data"
# $Id: $
