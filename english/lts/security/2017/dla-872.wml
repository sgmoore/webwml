<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that xrdp, a Remote Desktop Protocol (RDP) server,
calls the PAM function auth_start_session() in an incorrect location,
leading to PAM session modules not being properly initialized, with a
potential consequence of incorrect configurations or elevation of
privileges, aka a pam_limits.so bypass.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.5.0-2+deb7u1.</p>

<p>We recommend that you upgrade your xrdp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-872.data"
# $Id: $
