<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential memory corrution vulnerability
in python-cryptography, a Python library offering a number of encryption and
cryptography primitives.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23931">CVE-2023-23931</a>

    <p>cryptography is a package designed to expose cryptographic primitives and recipes to Python developers. In affected versions `Cipher.update_into` would accept Python objects which implement the buffer protocol, but provide only immutable buffers. This would allow immutable objects (such as `bytes`) to be mutated, thus violating fundamental rules of Python and resulting in corrupted output. This now correctly raises an exception. This issue has been present since `update_into` was originally introduced in cryptography 1.8.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
2.6.1-3+deb10u3.</p>

<p>We recommend that you upgrade your python-cryptography packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3331.data"
# $Id: $
