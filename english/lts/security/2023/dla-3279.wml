<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in trafficserver, a caching proxy
server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37150">CVE-2021-37150</a>

    <p>Improper Input Validation vulnerability in header parsing of
    Apache Traffic Server allows an attacker to request secure
    resources</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25763">CVE-2022-25763</a>

    <p>Improper Input Validation vulnerability in HTTP/2 request
    validation of Apache Traffic Server allows an attacker to create
    smuggle or cache poison attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28129">CVE-2022-28129</a>

    <p>Improper Input Validation vulnerability in HTTP/1.1 header parsing
    of Apache Traffic Server allows an attacker to send invalid
    headers</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31780">CVE-2022-31780</a>

    <p>Improper Input Validation vulnerability in HTTP/2 frame handling
    of Apache Traffic Server allows an attacker to smuggle requests.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
8.0.2+ds-1+deb10u7.</p>

<p>We recommend that you upgrade your trafficserver packages.</p>

<p>For the detailed security status of trafficserver please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/trafficserver">https://security-tracker.debian.org/tracker/trafficserver</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3279.data"
# $Id: $
